﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CmdLin.Parsing {
	/// <summary>
	/// The tape for reading string.
	/// </summary>
	public sealed class StringTape {
		/// <summary>
		/// Source string.
		/// </summary>
		public string Source { get; }

		private int _position;
		/// <summary>
		/// Current position of tape in source string.
		/// </summary>
		public int Position {
			get => this._position;
			set {
				if (value < 0 || value > this.Source.Length) {
					throw new ArgumentOutOfRangeException(nameof(value));
				}

				this._position = value;
			}
		}

		/// <summary>
		/// Returns true if tape is on the end of string.
		/// Othervise returns false.
		/// </summary>
		public bool EndOfString => this.Position >= this.Source.Length;

		/// <summary>
		/// Returns remaining lenght from current position to the end of source string.
		/// </summary>
		public int RemainingLength => Source.Length - this.Position;

		/// <summary>
		/// Returns character relative to current position.
		/// </summary>
		/// <param name="index">Relative index of character from current position.</param>
		/// <returns></returns>
		public char this[int index] {
			get => this.Source[this.Position + index];
		}

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="source">Source string of tape. Can not be null.</param>
		public StringTape(string source) : this(source, 0) { }

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="source">Source string of tape. Can not be null.</param>
		/// <param name="position">The start position of tape.</param>
		public StringTape(string source, int position) {
			if (source is null) { throw new ArgumentNullException(nameof(source)); }
			if (position < 0 || position > source.Length) {
				throw new ArgumentOutOfRangeException(nameof(position), $"The position must be withing range of passed string source.");
			}

			this.Source = source;
			this.Position = position;
		}

		/// <summary>
		/// Will move to the end of string.
		/// </summary>
		public void Finish() {
			this.Position = this.Source.Length;
		}

		/// <summary>
		/// Will move to the ned of string.
		/// </summary>
		/// <param name="text">Returns substring from original postion to the end.</param>
		public void Finish(out string text) {
			text = this.Source.Substring(this.Position);
			this.Finish();
		}

		private void ExtractPreviousText(int length, out string result) {
			if (length > 0) {
				result = this.Source.Substring(this.Position - length, length);
			}
			else {
				result = null;
			}
		}

		/// <summary>
		/// Returns true if current character is <paramref name="expectedChar"/>.
		/// Othervise returns false.
		/// </summary>
		/// <param name="expectedChar">Expected current character.</param>
		/// <returns></returns>
		public bool ContinuesWith(char expectedChar) {
			if (this.EndOfString) { return false; }

			return this[0] == expectedChar;
		}

		/// <summary>
		/// Returns true if string continues from current position with <paramref name="expectedString"/>.
		/// Othervise returns false.
		/// </summary>
		/// <param name="expectedString">Expected string.</param>
		/// <returns></returns>
		public bool ContinuesWith(string expectedString) {
			if (expectedString is null) { throw new ArgumentNullException(nameof(expectedString)); }
			if (expectedString.Length > this.RemainingLength) { return false; }

			for (var i = 0; i < expectedString.Length; i++) {
				var sourceChar = this.Source[i + this.Position];
				var expectedChar = expectedString[i];

				if (sourceChar != expectedChar) { return false; }
			}

			return true;
		}

		/// <summary>
		/// Will move current position forward by one if current character is same as <paramref name="expechtedChar"/>.
		/// Return true is position was moved.
		/// </summary>
		/// <param name="expechtedChar">Expected character.</param>
		/// <returns></returns>
		public bool SkipChar(char expechtedChar) {
			if (this.ContinuesWith(expechtedChar)) {
				this.Position++;
				return true;
			}

			return false;
		}

		/// <summary>
		/// Will move current position forward for lenght of <paramref name="expectedString"/> if type continues (<see cref="ContinuesWith(string)"/>) with <paramref name="expectedString"/>.
		/// Return number of skiped characters.
		/// </summary>
		/// <param name="expectedString">Expected string.</param>
		/// <returns></returns>
		public bool SkipString(string expectedString) {
			if (this.ContinuesWith(expectedString)) {
				this.Position += expectedString.Length;
				return true;
			}

			return false;
		}

		/// <summary>
		/// Will move current position forward for lenght of <paramref name="expectedString"/> if type continues (<see cref="ContinuesWith(string)"/>) with <paramref name="expectedString"/>.
		/// Return number of skiped characters.
		/// </summary>
		/// <param name="expectedString">Expected string.</param>
		/// <param name="text">Returns skiped text.</param>
		/// <returns></returns>
		public int SkipWhiteSpace(out string text) {
			var result = this.SkipWhiteSpace();
			this.ExtractPreviousText(result, out text);

			return result;
		}

		/// <summary>
		/// Will move current position forward until current character will not be white space.
		/// Returns number of skiped characters.
		/// </summary>
		/// <returns></returns>
		public int SkipWhiteSpace() => this.SkipUntil(ch => char.IsWhiteSpace(ch));

		/// <summary>
		/// Will move current position forward until current character will be white space.
		/// Returns number of skiped characters.
		/// </summary>
		/// <param name="text">Returns skiped text.</param>
		/// <returns></returns>
		public int SkipNonWhiteSpace(out string text) {
			var result = this.SkipNonWhiteSpace();
			this.ExtractPreviousText(result, out text);

			return result;
		}

		/// <summary>
		/// Will move current position forward until current character will be white space.
		/// Returns number of skiped characters.
		/// </summary>
		/// <returns></returns>
		public int SkipNonWhiteSpace() => this.SkipUntil(ch => !char.IsWhiteSpace(ch));

		/// <summary>
		/// Will move current position forward until <paramref name="predicate"/> returns true.
		/// Returns number of skiped characters.
		/// </summary>
		/// <param name="text">Returns skiped text.</param>
		/// <returns></returns>
		public int SkipUntil(Func<char, bool> predicate, out string text) {
			var result = this.SkipUntil(predicate);
			this.ExtractPreviousText(result, out text);

			return result;
		}

		/// <summary>
		/// Will move current position forward until <paramref name="predicate"/> returns true.
		/// Returns number of skiped characters.
		/// </summary>
		/// <returns></returns>
		public int SkipUntil(Func<char, bool> predicate) {
			int counter = 0;
			for (var i = this.Position; i < this.Source.Length; i++) {
				var ch = this.Source[i];
				if (!predicate(ch)) {
					this.Position = i;
					return counter;
				}

				counter++;
			}

			this.Position = this.Source.Length;
			return counter;
		}

		/// <summary>
		/// Will move current position forward until <paramref name="predicate"/> returns true.
		/// Returns number of skiped characters.
		/// </summary>
		/// <param name="text">Returns skiped text.</param>
		/// <returns></returns>
		public int SkipUntil(Func<string, int, bool> predicate, out string text) {
			var result = this.SkipUntil(predicate);
			this.ExtractPreviousText(result, out text);

			return result;
		}

		/// <summary>
		/// Will move current position forward until <paramref name="predicate"/> returns true.
		/// Returns number of skiped characters.
		/// </summary>
		/// <returns></returns>
		public int SkipUntil(Func<string, int, bool> predicate) {
			int counter = 0;
			for (var i = this.Position; i < this.Source.Length; i++) {
				if (!predicate(this.Source, i)) {
					this.Position = i;
					return counter;
				}

				counter++;
			}

			this.Position = this.Source.Length;
			return counter;
		}
	}
}

