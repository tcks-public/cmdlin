﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CmdLin.Parsing {
	/// <summary>
	/// Can parse <see cref="CmdToken"/> from string.
	/// </summary>
	public static class CmdParser {
		public static IEnumerable<CmdToken> ParseTokens(string input) {
			if (input is null) {
				return Enumerable.Empty<CmdToken>();
			}

			var tape = new StringTape(input);
			return ParseTokens(tape);
		}

		public static IEnumerable<CmdToken> ParseTokens(StringTape input) {
			while (!input.EndOfString) {
				input.SkipWhiteSpace();
				if (input.EndOfString) { yield break; }

				input.SkipUntil(ch => ch == '-', out var strPrefix);
				yield return ParseVerb(input, strPrefix);
			}
		}

		public static CmdToken ParseVerb(StringTape input, string prefix) {
			if (input.EndOfString) {
				return CmdToken.NewValid(prefix, null);
			}

			var ch0 = input[0];
			if (ch0 == '"' || ch0 == '\'' || ch0 == '`' || ch0 == '´') {
				return ParseVerb(input, prefix, ch0);
			}

			input.SkipUntil(ch => !char.IsWhiteSpace(ch), out var text);
			return CmdToken.NewValid(prefix, text);
		}

		public static CmdToken ParseVerb(StringTape input, string prefix, char quote) {
			if (input.EndOfString) {
				return CmdToken.NewInvalid(prefix);
			}

			if (!input.SkipChar(quote)) {
				return CmdToken.NewInvalid(prefix);
			}
			input.SkipUntil((str, ndx) => {
				return !(str[ndx] == quote && str[ndx - 1] != '\\');
			}, out var validText);

			if (validText is null) {
				// no finish quote
				input.Finish(out var remainingText);
				return CmdToken.NewInvalid($"{prefix}{quote}{remainingText}");
			}

			input.Position++;

			var cleanText = validText.Replace($"\\{quote}", $"{quote}");
			return CmdToken.NewValid(prefix, cleanText, $"{prefix}{quote}{validText}{quote}");
		}
	}
}
