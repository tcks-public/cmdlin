﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CmdLin {
	/// <summary>
	/// Represent one token int command.
	/// </summary>
	[System.Diagnostics.DebuggerDisplay("{FullOriginalText}")]
	public sealed class CmdToken {
		/// <summary>
		/// Creates new invalid <see cref="CmdToken"/>.
		/// </summary>
		/// <param name="fullOriginalText">See <see cref="CmdToken.FullOriginalText"/>.</param>
		/// <returns></returns>
		public static CmdToken NewInvalid(string fullOriginalText) {
			return new CmdToken(false, null, null, fullOriginalText);
		}

		public static CmdToken NewValid(string text) {
			return new CmdToken(true, null, text, text);
		}

		public static CmdToken NewValid(string prefix, string text) {
			if (prefix is null) {
				return NewValid(null, text, text);
			}
			if (text is null) {
				return NewValid(prefix, null, prefix);
			}

			return new CmdToken(true, prefix, text, $"{prefix}{text}");
		}

		public static CmdToken NewValid(string prefix, string text, string fullOriginalText) {
			return new CmdToken(true, prefix, text, fullOriginalText);
		}

		/// <summary>
		/// Returns true if token is valid.
		/// Othervise returns false.
		/// </summary>
		public bool IsValid { get; }

		/// <summary>
		/// Prefix of token (usullay '-' or '--').
		/// Null means no prefix.
		/// </summary>
		public string Prefix { get; }

		/// <summary>
		/// Parsed text of token with resolved escape sequences.
		/// </summary>
		public string Text { get; }

		/// <summary>
		/// Full original text with prefix and without resolved escape sequences.
		/// </summary>
		public string FullOriginalText { get; }

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="isValid">See <see cref="IsValid"/>.</param>
		/// <param name="prefix">See <see cref="Prefix"/>.</param>
		/// <param name="text">See <see cref="Text"/>.</param>
		/// <param name="fullOriginalText">See <see cref="FullOriginalText"/>.</param>
		public CmdToken(bool isValid, string prefix, string text, string fullOriginalText) {
			this.IsValid = isValid;
			this.Prefix = prefix;
			this.Text = text;
			this.FullOriginalText = fullOriginalText;
		}
	}
}
