﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using CmdLin.Parsing;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CmdLin.TESTS {
	[Microsoft.VisualStudio.TestTools.UnitTesting.TestClass]
	public class CmdParser_Tests {
		[TestMethod]
		public void CanParseNullString() {
			var tokens = CmdParser.ParseTokens((string)null).ToArray();

			Assert.IsTrue(tokens.Length == 0);
		}

		[TestMethod]
		public void CanParseEmptyString() {
			var tokens = CmdParser.ParseTokens(string.Empty).ToArray();

			Assert.IsTrue(tokens.Length == 0);
		}

		[TestMethod]
		public void CanParseWhitespaceString() {
			var tokens = CmdParser.ParseTokens(" \t\n\r").ToArray();

			Assert.IsTrue(tokens.Length == 0);
		}

		[TestMethod]
		public void CanParseOneSimpleVerb() {
			var inputs = new string[] { "hello", " hello", "hello ", " hello " };

			foreach (var input in inputs) {
				var tokens = CmdParser.ParseTokens(input).ToArray();

				Assert.IsTrue(tokens.Length == 1);
				Assert.IsTrue(tokens[0].Prefix == null);
				Assert.IsTrue(tokens[0].Text == "hello");
				Assert.IsTrue(tokens[0].FullOriginalText == "hello");
			}
		}

		[TestMethod]
		public void CanParseTwoSimpleVerbs() {
			var inputs = new string[] { "hello world", " hello  world", "hello\tworld ", " hello\r\nworld\t" };

			foreach (var input in inputs) {
				var tokens = CmdParser.ParseTokens(input).ToArray();

				Assert.IsTrue(tokens.Length == 2);

				Assert.IsTrue(tokens[0].Prefix == null);
				Assert.IsTrue(tokens[0].Text == "hello");
				Assert.IsTrue(tokens[0].FullOriginalText == "hello");

				Assert.IsTrue(tokens[1].Prefix == null);
				Assert.IsTrue(tokens[1].Text == "world");
				Assert.IsTrue(tokens[1].FullOriginalText == "world");
			}
		}

		[TestMethod]
		public void CanParseTwoSimpleVerbsSplittedByManyWhitespaces() {
			var tokens = CmdParser.ParseTokens("hello \t\r\nworld").ToArray();

			Assert.IsTrue(tokens.Length == 2);

			Assert.IsTrue(tokens[0].Prefix == null);
			Assert.IsTrue(tokens[0].Text == "hello");
			Assert.IsTrue(tokens[0].FullOriginalText == "hello");

			Assert.IsTrue(tokens[1].Prefix == null);
			Assert.IsTrue(tokens[1].Text == "world");
			Assert.IsTrue(tokens[1].FullOriginalText == "world");
		}

		[TestMethod]
		public void CanParseOneMultiVerb() {
			var inputs = new string[] { "\"hello world\"", " \"hello world\" ", "\r\n\"hello world\"\t\r\n" };

			foreach (var input in inputs) {
				var tokens = CmdParser.ParseTokens(input).ToArray();

				Assert.IsTrue(tokens.Length == 1);

				Assert.IsTrue(tokens[0].Prefix == null);
				Assert.IsTrue(tokens[0].Text == "hello world");
				Assert.IsTrue(tokens[0].FullOriginalText == "\"hello world\"");
			}
		}

		[TestMethod]
		public void CanParseManyMultiVerbs() {
			var corners = new string[] {
				null, string.Empty, " ", "\t", "\r", "\n", " \t\r\n "
			};
			var verbs = new string[] {
				null, "hello", "world", "aloha"
			};

			var joins = corners.Where(x => !string.IsNullOrEmpty(x));
			var multiverbs = (from a in verbs
							  from sa in joins
							  from b in verbs.Where(x => !string.IsNullOrEmpty(x))
							  where b != a
							  from sb in joins
							  from c in verbs.Where(x => !string.IsNullOrEmpty(x))
							  where c != b && c != a
							  select $"{a}{sa}{b}{sb}{c}").ToArray();

			IEnumerable<(string text, CmdToken[] tokens)> GetInputs(int skip) {
				foreach (var multiverb in multiverbs.Skip(skip)) {
					foreach (var startCorner in corners) {
						foreach (var endCorner in corners) {
							var token0 = CmdToken.NewValid(null, multiverb, $"\"{multiverb}\"");
							var text0 = $"{startCorner}{token0.FullOriginalText}{endCorner}";

							var arrToken0 = new[] { token0 };

							foreach (var subResult in GetInputs(skip + 1)) {
								var text1 = text0 + subResult.text;
								var arrToken1 = arrToken0.Concat(subResult.tokens).ToArray();
								yield return (text1, arrToken1);
							}

							yield return (text0, arrToken0);
						}
					}
				}
			}

			var inputs = GetInputs(0).Take(1000).ToArray();
			foreach (var pair in inputs) {
				var tokens = CmdParser.ParseTokens(pair.text).ToArray();

				Assert.IsTrue(tokens.Length == pair.tokens.Length);

				for (var i = 0; i < tokens.Length; i++) {
					Assert.IsTrue(tokens[i].IsValid == pair.tokens[i].IsValid);
					Assert.IsTrue(tokens[i].Prefix == pair.tokens[i].Prefix);
					Assert.IsTrue(tokens[i].Text == pair.tokens[i].Text);
					Assert.IsTrue(tokens[i].FullOriginalText == pair.tokens[i].FullOriginalText);
				}
			}
		}

		[TestMethod]
		public void CanParseOneFullFlag() {
			var tokens = CmdParser.ParseTokens("--flag").ToArray();

			AssertCmdToken(tokens[0], CmdToken.NewValid("--", "flag"));
		}

		[TestMethod]
		public void CanParseTwoFullFlags() {
			var tokens = CmdParser.ParseTokens("--flag --message").ToArray();

			Assert.AreEqual(2, tokens.Length);

			AssertCmdToken(tokens[0], CmdToken.NewValid("--", "flag"));
			AssertCmdToken(tokens[1], CmdToken.NewValid("--", "message"));
		}

		[TestMethod]
		public void CanParseOneShortFlag() {
			var tokens = CmdParser.ParseTokens("-flag").ToArray();

			Assert.AreEqual(1, tokens.Length);

			AssertCmdToken(tokens[0], CmdToken.NewValid("-", "flag"));
		}

		[TestMethod]
		public void CanParseTwoFullParameters() {
			var tokens = CmdParser.ParseTokens("--name myname --description \"this is long description\"").ToArray();

			Assert.AreEqual(4, tokens.Length);

			AssertCmdToken(tokens[0], CmdToken.NewValid("--", "name"));
			AssertCmdToken(tokens[1], CmdToken.NewValid("myname"));
			AssertCmdToken(tokens[2], CmdToken.NewValid("--", "description"));
			AssertCmdToken(tokens[3], CmdToken.NewValid(null, "this is long description", @"""this is long description"""));
		}

		[TestMethod]
		public void CanParseQuotedParameters() {
			var tokens = CmdParser.ParseTokens(@"first -second --third ---fourth -'fi ft' --""six th"" ---`se ve nth` ----´ni n t h´").ToArray();

			Assert.AreEqual(8, tokens.Length);

			AssertCmdToken(tokens[0], CmdToken.NewValid("first"));
			AssertCmdToken(tokens[1], CmdToken.NewValid("-", "second"));
			AssertCmdToken(tokens[2], CmdToken.NewValid("--", "third"));
			AssertCmdToken(tokens[3], CmdToken.NewValid("---", "fourth"));
			AssertCmdToken(tokens[4], CmdToken.NewValid("-", "fi ft", "-'fi ft'"));
			AssertCmdToken(tokens[5], CmdToken.NewValid("--", "six th", @"--""six th"""));
			AssertCmdToken(tokens[6], CmdToken.NewValid("---", "se ve nth", "---`se ve nth`"));
			AssertCmdToken(tokens[7], CmdToken.NewValid("----", "ni n t h", "----´ni n t h´"));
		}

		[TestMethod]
		public void CanParseEscapedQuotesParameters() {
			var tokens = CmdParser.ParseTokens(@"first -second --third ---fourth -'fi\'ft' --""six\""th"" ---`se\`ve\`nth` ----´ni\´n\´t\´h´").ToArray();

			Assert.AreEqual(8, tokens.Length);

			AssertCmdToken(tokens[0], CmdToken.NewValid("first"));
			AssertCmdToken(tokens[1], CmdToken.NewValid("-", "second"));
			AssertCmdToken(tokens[2], CmdToken.NewValid("--", "third"));
			AssertCmdToken(tokens[3], CmdToken.NewValid("---", "fourth"));
			AssertCmdToken(tokens[4], CmdToken.NewValid("-", "fi'ft", @"-'fi\'ft'"));
			AssertCmdToken(tokens[5], CmdToken.NewValid("--", @"six""th", @"--""six\""th"""));
			AssertCmdToken(tokens[6], CmdToken.NewValid("---", "se`ve`nth", @"---`se\`ve\`nth`"));
			AssertCmdToken(tokens[7], CmdToken.NewValid("----", "ni´n´t´h", @"----´ni\´n\´t\´h´"));
		}

		[TestMethod]
		public void CanParseNestedQuotes() {
			var tokens = CmdParser.ParseTokens(@"'a""b`c´d""""e``f´´g' ""a'b`c´d''e``f´´g""").ToArray();

			Assert.AreEqual(2, tokens.Length);

			AssertCmdToken(tokens[0], CmdToken.NewValid(null, @"a""b`c´d""""e``f´´g", @"'a""b`c´d""""e``f´´g'"));
			AssertCmdToken(tokens[1], CmdToken.NewValid(null, @"a'b`c´d''e``f´´g", @"""a'b`c´d''e``f´´g"""));
		}

		private static void AssertCmdToken(CmdToken expected, CmdToken actual) {
			if (object.ReferenceEquals(expected, actual)) { return; }

			if (expected is null && actual is object) {
				Assert.Fail("Actual token is null but expected token is object.");
			}
			if (expected is object && actual is null) {
				Assert.Fail("Actual token is object but expected token is null.");
			}

			AssertEquality(expected, actual
				, x => x.IsValid
				, x => x.Prefix
				, x => x.Text
				, x => x.FullOriginalText
			);
		}

		private static void AssertEquality<T>(T expected, T actual, params Expression<Func<T, object>>[] expressions) {
			if (expressions is null) { return; }

			foreach (var expression in expressions) {
				if (expression is null) { continue; }

				AssertEquality<T>(expected, actual, expression);
			}
		}
		private static void AssertEquality<T>(T expected, T actual, Expression<Func<T, object>> expression) {
			var valueProvider = expression.Compile();
			var expectedValue = valueProvider(expected);
			var actualValue = valueProvider(actual);

			var message = $"({expectedValue}) != ({actualValue})\n{expression.Body}";
			Assert.AreEqual(expectedValue, actualValue, message);
		}
	}
}
